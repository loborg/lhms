package org.finalproject.lhms;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.finalproject.lhms.controller.LoginPageController;
import org.finalproject.lhms.controller.form.LoginForm;
import org.finalproject.lhms.controller.validator.LoginValidator;
import org.finalproject.lhms.service.HouseholdService;
import org.finalproject.lhms.service.MemberService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


@RunWith(SpringRunner.class)
@WebMvcTest(LoginPageController.class)
public class LoginPageControllerTest {
	
	private static final String LOGIN_PAGE_VIEW = "login-page";

	private static final String CONTENTTYPE_HTML_UTF8 = "text/html;charset=UTF-8";

	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private LoginValidator formValidator;
	
	@MockBean
	private MemberService memberService;
	
	@MockBean
	private HouseholdService householdService;
	
	@Before
	public void initMock() {
		Mockito.when(formValidator.supports(LoginForm.class)).thenReturn(true);
	}

	@Test
	public void testGetLogin() throws Exception {
		mvc.perform(get("/").accept(MediaType.TEXT_HTML))
		.andExpect(content().contentType(CONTENTTYPE_HTML_UTF8))
		.andExpect(status().isOk())
		.andExpect(view().name(LOGIN_PAGE_VIEW))
		.andExpect(model().attributeExists("loginForm"))
		.andExpect(model().attributeDoesNotExist("member"));		
	}
	
	private LoginForm givenBlankUserNameAndPassword() {
		final LoginForm formInput = new LoginForm();
		formInput.setLoginName("");
		formInput.setPassword("");
		return formInput;
	}
	
	@Test
	public void testLoginWhitEmptyValues() throws Exception {
		final LoginForm formInput = givenBlankUserNameAndPassword();
		
		mvc.perform(get("/").accept(MediaType.TEXT_HTML)
				.param("loginName", formInput.getLoginName())
				.param("password", formInput.getPassword()))
		.andDo(print())
		.andExpect(content().contentType(CONTENTTYPE_HTML_UTF8))
		.andExpect(status().isOk())
		.andExpect(view().name(LOGIN_PAGE_VIEW));
	}
}
