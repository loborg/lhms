package org.finalproject.lhms;

import static org.assertj.core.api.Assertions.assertThat;

import org.finalproject.lhms.controller.LandingPageController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class LandingPageControllerTest {
	
	private static final String LANDING_PAGE_VIEW = "landing-page";
	
	@InjectMocks
	private LandingPageController controller;
	
	@Test
	public void testGetLandingPageShouldReturnLandingPageWhenAccessingDocumentRoot() {
		assertThat(controller.getLandingPage()).isEqualTo(LANDING_PAGE_VIEW);
	}

}
