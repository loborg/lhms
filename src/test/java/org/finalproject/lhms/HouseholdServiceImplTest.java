package org.finalproject.lhms;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.finalproject.lhms.model.Household;
import org.finalproject.lhms.repository.HouseholdRepository;
import org.finalproject.lhms.service.impl.HouseholdServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class HouseholdServiceImplTest {
	
	@Mock
	private HouseholdRepository householdRepo;
	
	@InjectMocks
	private HouseholdServiceImpl householdService;
	
	private List<Household> householdList;
	
	private List<Integer> householdMembers;
	
	@Before
	public void givenHouseholdRepository() {
		householdList = new ArrayList<>();
		householdList.add(new Household(1, "hhName1", "hhAdress1", 2, "GUID1"));
		householdList.add(new Household(2, "hhName2", "hhAdress2", 5, "GUID2"));
	}
	
	public List<Household> givenMoreThenOneHouseholdInRepository() {
		Mockito.when(householdRepo.findAll()).thenReturn(householdList);
		return householdList;
	}
	
	public List<Integer> givenMoreThenOneMemberInLastlyAddedHousehold(){
		givenMoreThenOneHouseholdInRepository();
		householdMembers = new ArrayList<>();
		int memberCount = householdService.getLastlyAddedHousehold().getHouseholdMemberCount();
		for (int i = 1; i <= memberCount; i++) {
			householdMembers.add(i);
		}
		return householdMembers;
	}
	
	@Test
	public void testGetLastlyAddedHousehold() {
		List<Household> listInput = givenMoreThenOneHouseholdInRepository();
		assertThat(householdService.getLastlyAddedHousehold()).isEqualTo(listInput.get(listInput.size()-1));
		Mockito.verify(householdRepo, Mockito.times(1)).findAll();
	}
	
	@Test
	public void testGetHouseholdMemberCount() {
		List<Integer> listInput = givenMoreThenOneMemberInLastlyAddedHousehold();
		assertThat(householdService.getHouseholdMemberCount()).isEqualTo(listInput);
		Mockito.verify(householdRepo, Mockito.times(2)).findAll();
	}
	
	@Test
	public void testFindHouseholdById() {
		List<Household> listInput = givenMoreThenOneHouseholdInRepository();
		assertThat(householdService.findHouseholdById(householdService.getLastlyAddedHousehold().getHouseholdId()).getHouseholdId())
		.isEqualTo(listInput.get(listInput.size()-1).getHouseholdId());
	}
}
