package org.finalproject.lhms.repository;

import org.finalproject.lhms.model.Financial;
import org.springframework.stereotype.Repository;

@Repository
public interface FinancialRepository extends CRUDRepository<Financial, String> {

}
