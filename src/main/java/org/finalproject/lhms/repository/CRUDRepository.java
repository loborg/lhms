package org.finalproject.lhms.repository;

import java.util.List;

//General CRUDRepository
public interface CRUDRepository<E, K> {
	void create(E e);
	E getById(K id);
	List<E> findAll();
	void update(E e);
	void deleteById(K id);
}
