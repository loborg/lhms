package org.finalproject.lhms.repository;

import java.util.List;

import org.finalproject.lhms.model.Member;

public interface MemberRepository extends CRUDRepository<Member, String> {
	
	/**
	 * This method finds all the member from the household whit the given householdId
	 * @param*/
	List<Member> findMembersInHouseholdById(int householdId);
	
	/**
	 * This method will delete all the members whit the given householdID
	 * @param*/
	void deleteMemberByHouseholdId(int householdId);

}
