package org.finalproject.lhms.repository.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import org.finalproject.lhms.model.Household;
import org.finalproject.lhms.model.Member;
import org.finalproject.lhms.repository.MemberRepository;
import org.finalproject.lhms.service.HouseholdService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

@Repository
public class MemberRepositoryImpl implements MemberRepository {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private HouseholdService householdService;
	
	private RowMapper<Member> rowMapper = new RowMapper<Member>() {
			
			@Override
			public Member mapRow(ResultSet rs, int rowNum) throws SQLException {
				Member member = new Member();
				member.setMemberId(rs.getInt("memberId"));
				member.setHouseholdId(rs.getInt("householdId"));
				member.setFirstName(rs.getString("firstName"));
				member.setLastName(rs.getString("lastName"));
				member.setPhoneNumber(rs.getString("phoneNumber"));
				member.setEmail(rs.getString("email"));
				member.setNetWage(rs.getLong("netWage"));
				member.setUserName(rs.getString("userName"));
				member.setPassword(rs.getString("password"));
				member.setMemberGUID(rs.getString("memberGUID"));
				return member;
			}
		};
	
	@Override
	public void create(Member e) {
		Assert.notNull(e, "Member can not be null!");
		Household household = householdService.getLastlyAddedHousehold();
		String GUID = UUID.randomUUID().toString();
		
		
		jdbcTemplate.update(
				"INSERT INTO member ("
				+ "memberId, "
				+ "householdId, "
				+ "firstName, "
				+ "lastName, "
				+ "phoneNumber, "
				+ "email, "
				+ "netWage, "
				+ "userName, "
				+ "password, "
				+ "memberGUID)"
				+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);",
				e.getMemberId(), 
				household.getHouseholdId(), 
				e.getFirstName(), 
				e.getLastName(), 
				e.getPhoneNumber(),
				e.getEmail(), 
				e.getNetWage(), 
				e.getUserName(), 
				e.getPassword(), 
				GUID);
	}

	@Override
	public Member getById(String id) {
		Assert.notNull(id, "Household id can not be null");
		
		Member member = jdbcTemplate.queryForObject(""
				+ "SELECT "
				+ "memberId, "
				+ "householdId, "
				+ "firstName, "
				+ "lastName, "
				+ "phoneNumber, "
				+ "email, "
				+ "netWage, "
				+ "userName, "
				+ "password, "
				+ "memberGUID "
				+ "FROM member "
				+ "WHERE memberGUID = ?", new Object[] {id}, rowMapper);
		if (member == null) {
			throw new EmptyResultDataAccessException(1);
		} else {
			return member;
		}
	}

	@Override
	public List<Member> findAll() {
		return jdbcTemplate.query(""
				+ "SELECT "
				+ "memberId, "
				+ "householdId, "
				+ "firstName, "
				+ "lastName, "
				+ "phoneNumber, "
				+ "email, "
				+ "netWage, "
				+ "userName, "
				+ "password, "
				+ "memberGUID "
				+ "FROM member", new Object[] {}, rowMapper);
	}

	@Override
	public void update(Member e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteById(String id) {

	}

	@Override
	public List<Member> findMembersInHouseholdById(int householdId) {
		Assert.notNull(householdId, "Household Id can not be null!");
		return jdbcTemplate.query(""
				+ "SELECT "
				+ "memberId, "
				+ "householdId, "
				+ "firstName, "
				+ "lastName, "
				+ "phoneNumber, "
				+ "email, "
				+ "netWage, "
				+ "userName, "
				+ "password, "
				+ "memberGUID "
				+ "FROM member "
				+ "WHERE householdId = ?", new Object[] {householdId}, rowMapper);
	}

	@Override
	public void deleteMemberByHouseholdId(int householdId) {
		Assert.notNull(householdId, "The Household ID can net be null!");
		jdbcTemplate.update("DELETE FROM member WHERE householdId = ?", householdId);
	}

}
