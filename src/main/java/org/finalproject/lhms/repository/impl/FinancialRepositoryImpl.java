package org.finalproject.lhms.repository.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.finalproject.lhms.model.Financial;
import org.finalproject.lhms.repository.FinancialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

@Repository
public class FinancialRepositoryImpl implements FinancialRepository {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private RowMapper<Financial> rowMapper = new RowMapper<Financial>() {

		@Override
		public Financial mapRow(ResultSet rs, int rowNum) throws SQLException {
			Financial financial = new Financial();
			financial.setItemId(rs.getInt("itemId"));
			financial.setIntakeDate(rs.getDate("intakeDate"));
			financial.setIncomeItem(rs.getString("incomeItem"));
			financial.setIncomeAmount(rs.getInt("incomeAmount"));
			financial.setExpenseItem(rs.getString("expenseItem"));
			financial.setExpenseAmount(rs.getInt("expenseAmount"));
			financial.setMemberGUID(rs.getString("memberGUID"));
			return financial;
		}
	};

	@Override
	public void create(Financial e) {
		Assert.notNull(e, "Entity can not be null!");		
		jdbcTemplate.update(""
				+ "INSERT INTO financial ("
				+ "intakeDate, "
				+ "incomeItem, "
				+ "incomeAmount, "
				+ "expenseItem, "
				+ "expenseAmount, "
				+ "memberGUID) "
				+ "VALUES(?, ?, ?, ?, ?, ?);", 
				e.getIntakeDate(), 
				e.getIncomeItem(),
				e.getIncomeAmount(),
				e.getExpenseItem(),
				e.getExpenseAmount(),
				e.getMemberGUID());
	}

	@Override
	public Financial getById(String id) {
		return null;
	}

	@Override
	public List<Financial> findAll() {
		return jdbcTemplate.query(
				"SELECT "
				+ "itemId, "
				+ "intakeDate, "
				+ "incomeItem, "
				+ "incomeAmount, "
				+ "expenseItem, "
				+ "expenseAmount, "
				+ "memberGUID "
				+ "FROM financial", new Object[] {}, rowMapper);
	}

	@Override
	public void update(Financial e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteById(String id) {
		Assert.notNull(id, "The Household ID can net be null!");
		jdbcTemplate.update("DELETE FROM financial WHERE memberGUID = ?", id);
	}

}
