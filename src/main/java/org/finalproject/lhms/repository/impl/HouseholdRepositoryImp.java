package org.finalproject.lhms.repository.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import org.finalproject.lhms.model.Household;
import org.finalproject.lhms.repository.HouseholdRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

@Repository
public class HouseholdRepositoryImp implements HouseholdRepository {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private RowMapper<Household> rowMapper = new RowMapper<Household>() {
		
		@Override
		public Household mapRow(ResultSet rs, int rowNum) throws SQLException {
			Household house = new Household();
			house.setHouseholdId(rs.getInt("householdId"));
			house.setHouseholdName(rs.getString("householdName"));
			house.setHouseholdAddress(rs.getString("householdAddress"));
			house.setHouseholdMemberCount(rs.getInt("householdMemberCount"));
			house.setHouseholdGUID(rs.getString("householdGUID"));
			return house;
		}
	};

	/**
	 * Creating one member of the household table
	 * @param */
	@Override
	public void create(Household e) {
		Assert.notNull(e, "Household can not be null!");
		
		String GUID = UUID.randomUUID().toString(); //Generating a random unique GUID for the household
		
		jdbcTemplate.update(
				"INSERT INTO household ("
				+ "householdName, "
				+ "householdAddress, "
				+ "householdMemberCount, "
				+ "householdGUID) "
				+ "VALUES(?, ?, ?, ?);",  
				e.getHouseholdName(), 
				e.getHouseholdAddress(), 
				e.getHouseholdMemberCount(),
				GUID);
	}

	@Override
	public Household getById(String id) {
		return jdbcTemplate.queryForObject(
				"SELECT "
				+ "householdId, "
				+ "householdName, "
				+ "householdAddress, "
				+ "householdMemberCount, "
				+ "householdGUID "
				+ "FROM "
				+ "household "
				+ "WHERE householdGUID = ?", new Object[] {id}, rowMapper);
	}

	@Override
	public List<Household> findAll() {
		return jdbcTemplate.query("SELECT householdId, "
				+ "householdName, "
				+ "householdAddress, "
				+ "householdMemberCount, "
				+ "householdGUID "
				+ "FROM household", new Object[] {}, rowMapper);
	}

	@Override
	public void update(Household e) {
		Assert.notNull(e, "The Entity can not be null!");
		jdbcTemplate.update(
				"UPDATE household SET "
				+ "householdName = ?, "
				+ "householdAddress = ?, "
				+ "householdMemberCount = ?, "
				+ "householdGUID = ? "
				+ "WHERE householdId = ? ", 
				e.getHouseholdName(), 
				e.getHouseholdAddress(), 
				e.getHouseholdMemberCount(), 
				e.getHouseholdGUID(), 
				e.getHouseholdId());
	}

	@Override
	public void deleteById(String GUID) {
		Assert.notNull(GUID, "The id can not be null");
		jdbcTemplate.update("DELETE FROM household WHERE householdGUID = ?", GUID);
	}
}
