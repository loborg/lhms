package org.finalproject.lhms.repository;

import org.finalproject.lhms.model.Household;

public interface HouseholdRepository extends CRUDRepository<Household, String> {
	
}
