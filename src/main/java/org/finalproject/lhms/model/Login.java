package org.finalproject.lhms.model;

public class Login {
	
	private String loginName;
	private String password;
	
	public Login() {}

	public Login(String loginName, String password) {
		this.loginName = loginName;
		this.password = password;
	}

	public String getLoginName() {
		return loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
