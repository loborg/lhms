package org.finalproject.lhms.model;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class Financial {
	
	private int itemId;
	private Date intakeDate;
	private String incomeItem;
	private int incomeAmount;
	private String expenseItem;
	private int expenseAmount;
	private String memberGUID;
	
	public Financial() {}
	
	public Financial(int itemId, Date intakeDate, String incomeItem, int incomeAmount, String expenseItem,
			int expenseAmount, String memberGUID) {
		this.itemId = itemId;
		this.intakeDate = intakeDate;
		this.incomeItem = incomeItem;
		this.incomeAmount = incomeAmount;
		this.expenseItem = expenseItem;
		this.expenseAmount = expenseAmount;
		this.memberGUID = memberGUID;
	}

	public int getItemId() {
		return itemId;
	}

	public Date getIntakeDate() {
		return intakeDate;
	}

	public String getIncomeItem() {
		return incomeItem;
	}

	public int getIncomeAmount() {
		return incomeAmount;
	}

	public String getExpenseItem() {
		return expenseItem;
	}

	public int getExpenseAmount() {
		return expenseAmount;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public void setIntakeDate(Date intakeDate) {
		this.intakeDate = intakeDate;
	}

	public void setIncomeItem(String incomeItem) {
		this.incomeItem = incomeItem;
	}

	public void setIncomeAmount(int incomeAmount) {
		this.incomeAmount = incomeAmount;
	}

	public void setExpenseItem(String expenseItem) {
		this.expenseItem = expenseItem;
	}

	public void setExpenseAmount(int expenseAmount) {
		this.expenseAmount = expenseAmount;
	}

	public String getMemberGUID() {
		return memberGUID;
	}

	public void setMemberGUID(String memberGUID) {
		this.memberGUID = memberGUID;
	}
}
