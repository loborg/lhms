package org.finalproject.lhms.model;

import java.util.Date;

public class TableItem {
	
	private Date date;
	private String member;
	private int income;
	private int expense;
	
	public TableItem() {
		
	}

	public TableItem(Date date, String member, int income, int expense) {
		this.date = date;
		this.member = member;
		this.income = income;
		this.expense = expense;
	}

	public Date getDate() {
		return date;
	}

	public String getMember() {
		return member;
	}

	public int getIncome() {
		return income;
	}

	public int getExpense() {
		return expense;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setMember(String member) {
		this.member = member;
	}

	public void setIncome(int income) {
		this.income = income;
	}

	public void setExpense(int expense) {
		this.expense = expense;
	}
}
