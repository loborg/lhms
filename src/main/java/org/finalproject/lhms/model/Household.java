package org.finalproject.lhms.model;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Household {
	
	private int householdId;
	private String householdName;
	private String householdAddress;
	private int householdMemberCount;
	private String householdGUID;
	
	public Household() {
		
	}
	
	public Household(int householdId, String householdName, String householdAddress, int householdMemberCount,
			String householdGUID) {
		this.householdId = householdId;
		this.householdName = householdName;
		this.householdAddress = householdAddress;
		this.householdMemberCount = householdMemberCount;
		this.householdGUID = householdGUID;
	}


	public int getHouseholdId() {
		return householdId;
	}

	public String getHouseholdName() {
		return householdName;
	}

	public String getHouseholdAddress() {
		return householdAddress;
	}

	public int getHouseholdMemberCount() {
		return householdMemberCount;
	}

	public String getHouseholdGUID() {
		return householdGUID;
	}

	public void setHouseholdId(int householdId) {
		this.householdId = householdId;
	}

	public void setHouseholdName(String householdName) {
		this.householdName = householdName;
	}

	public void setHouseholdAddress(String householdAddress) {
		this.householdAddress = householdAddress;
	}

	public void setHouseholdMemberCount(int householdMemberCount) {
		this.householdMemberCount = householdMemberCount;
	}

	public void setHouseholdGUID(String householdGUID) {
		this.householdGUID = householdGUID;
	}
	
}
