package org.finalproject.lhms.model;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Member {
	
	private int memberId;
	private int householdId;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private String email;
	private String emailConfirmation;
	private boolean salary;
	private long netWage;
	private String userName;
	private String password;
	private String passwordConfirmation;
	private String memberGUID;
	
	public Member() {
		
	}

	public Member(int memberId, int householdId, String firstName, String lastName,
			String phoneNumber, String email, String emailConfirmation, boolean salary, long netWage, String userName,
			String password, String passwordConfirmation, String memberGUID) {
		this.memberId = memberId;
		this.householdId = householdId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.emailConfirmation = emailConfirmation;
		this.salary = salary;
		this.netWage = netWage;
		this.userName = userName;
		this.password = password;
		this.passwordConfirmation = passwordConfirmation;
		this.memberGUID = memberGUID;
	}

	public int getMemberId() {
		return memberId;
	}

	public int getHouseholdId() {
		return householdId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public String getEmailConfirmation() {
		return emailConfirmation;
	}

	public boolean isSalary() {
		return salary;
	}

	public long getNetWage() {
		return netWage;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	public String getMemberGUID() {
		return memberGUID;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public void setHouseholdId(int householdId) {
		this.householdId = householdId;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEmailConfirmation(String emailConfirmation) {
		this.emailConfirmation = emailConfirmation;
	}

	public void setSalary(boolean salary) {
		this.salary = salary;
	}

	public void setNetWage(long netWage) {
		this.netWage = netWage;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}

	public void setMemberGUID(String memberGUID) {
		this.memberGUID = memberGUID;
	}
}
