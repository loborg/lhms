package org.finalproject.lhms.service;

import java.util.List;

import org.finalproject.lhms.model.Financial;
import org.finalproject.lhms.model.TableItem;
import org.springframework.stereotype.Service;

@Service
public interface FinancialService {
	
	void saveFinancialData(Financial financial);
	
	List<Financial> findAllMemberByGUID(String memberGUID);
	
	List<TableItem> findAllTableItem(int householdId);
}
