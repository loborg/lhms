package org.finalproject.lhms.service.impl;

import static org.finalproject.lhms.Keys.BASE_INCOME_ITEM;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.finalproject.lhms.model.Financial;
import org.finalproject.lhms.model.Member;
import org.finalproject.lhms.model.TableItem;
import org.finalproject.lhms.repository.FinancialRepository;
import org.finalproject.lhms.repository.MemberRepository;
import org.finalproject.lhms.service.FinancialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FinancialServiceImp implements FinancialService {
	
	@Autowired
	private FinancialRepository financialRepo;
	
	@Autowired
	private MemberRepository memberRepo;

	@Override
	public void saveFinancialData(Financial financial) {
		String GUID = financial.getMemberGUID();
		Member member = memberRepo.getById(GUID);
		long baseIncome = member.getNetWage();
		List<Financial> storedFinancialItems = findAllMemberByGUID(GUID);
		List<String> incomeItems = new ArrayList<>();
		for (Financial f : storedFinancialItems) {
			incomeItems.add(f.getIncomeItem());
		}
		if (!incomeItems.contains(BASE_INCOME_ITEM)){
			financialRepo.create(financial);
			financial.setIncomeAmount((int) baseIncome);
			financial.setIncomeItem(BASE_INCOME_ITEM);
			financial.setExpenseItem("");
			financial.setExpenseAmount(0);
			financialRepo.create(financial);
		} else {
			financialRepo.create(financial);
		}
	}

	@Override
	public List<Financial> findAllMemberByGUID(String memberGUID) {
		List<Financial> allItem = financialRepo.findAll();
		List<Financial> allItemWhitSameId = new ArrayList<>();
		for (Financial f : allItem) {
			if (f.getMemberGUID().equals(memberGUID)) {
				allItemWhitSameId.add(f);
			}
		}
		return allItemWhitSameId;
	}

	@Override
	public List<TableItem> findAllTableItem(int householdId) {
		List<TableItem> housholdMembersTableItems = new ArrayList<>();
		for (Member h : memberRepo.findMembersInHouseholdById(householdId)) {
			for (Financial f : findAllMemberByGUID(h.getMemberGUID())) {
				Date date = f.getIntakeDate();
				String member = "";
				int income = f.getIncomeAmount();
				int expense = f.getExpenseAmount();
				for (Member m : memberRepo.findAll()) {
					if (m.getMemberGUID().equals(h.getMemberGUID())) {
						member = m.getUserName();
						break;
					}
				}
				TableItem tableItem = new TableItem(date, member, income, expense);
				housholdMembersTableItems.add(tableItem);
			}	
		}
		return housholdMembersTableItems;
	}
}
