package org.finalproject.lhms.service.impl;

import static org.finalproject.lhms.Keys.MEMBER_ALREADY_REGISTRATED_MESSAGE;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.finalproject.lhms.controller.form.MemberForm;
import org.finalproject.lhms.model.Member;
import org.finalproject.lhms.repository.MemberRepository;
import org.finalproject.lhms.service.HouseholdService;
import org.finalproject.lhms.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

@Service
public class MemberServiceImpl implements MemberService {
	
	@Autowired
	private MemberRepository memberRepo;
	
	@Autowired
	private HouseholdService householdService;
	
	@Resource
    private ConversionService conversionService;
	
	@Autowired
	private Member member;

	@Override
	public String encriptPassword(String password) {
		int forignKey = 5;
		StringBuilder changed = new StringBuilder();
		List<Character> charList = new ArrayList<Character>();
		char[] chars = password.toCharArray();
		for (Character c : chars) {
			charList.add(c);
		}
		
		int index = 0;
		for (Character c : charList) {
			int charValue = Integer.valueOf(c);
			if (charValue >= 65 || charValue <= 90) {
				int shift = charValue + forignKey;
				if (shift > 90) {
					shift -= 26;
				}
				c = (char) (shift);
				if (index%2 != 0) {
					c = (char) (c + 32);
				}
				charList.set(index, c);
			} else if (charValue >= 48 || charValue <= 57) {
				c = (char) ((9 - (charValue - 48)) + 48); 
				charList.set(index, c);
			}
			index++;
		}
		
		Collections.reverse(charList);
		
		for (Character c : charList) {
			changed.append(c);
		}
		return changed.toString();
	}

	@Override
	public void deleteMembersFromSpecificHousuold() {
		memberRepo.deleteMemberByHouseholdId(householdService.getLastlyAddedHousehold().getHouseholdId());
	}

	@Override
	public void examineMemberExistenceWithinHousehold(MemberForm memberForm, Errors errors) {
		List<Member> member = memberRepo
				.findMembersInHouseholdById(householdService
						.getLastlyAddedHousehold().getHouseholdId());
		for (Member h : member) {
			MemberForm hForm = conversionService.convert(h, MemberForm.class);
			if (memberForm.getMemberId() == hForm.getMemberId()) {
				errors.rejectValue("memberId", "error.existingMember", 
						new Object[] {hForm.getMemberId()}, 
						MEMBER_ALREADY_REGISTRATED_MESSAGE);
			}
		}
	}

	@Override
	public void usernameExistenceIspection(MemberForm memberForm, Errors errors) {
		List<Member> allMember = memberRepo.findAll();
		for (Member m : allMember) {
			MemberForm mForm = conversionService.convert(m, MemberForm.class);
			if (memberForm.getUserName().equals(mForm.getUserName())) {
				errors.rejectValue("userName", "error.sameUserName", null, "The user name is already exists!");
			}
		}
		
	}

	@Override
	public void saveHouseholdMemberToDatabase(Member member) {
		member.setPassword(encriptPassword(member.getPassword()));
		memberRepo.create(member);
	}

	@Override
	public void emailExistenceIspection(MemberForm memberForm, Errors errors) {
		List<Member> allMember = memberRepo.findAll();
		for (Member m : allMember) {
			MemberForm mForm = conversionService.convert(m, MemberForm.class);
			if (memberForm.getEmail().equals(mForm.getEmail())) {
				errors.rejectValue("email", "error.sameEmail", null, "The user name is already exists!");
			}
		}
		
	}

	@Override
	public Member findMemberByUserName(String username) {
		List<Member> allMembers = memberRepo.findAll();
		for (Member m : allMembers) {
			if (m.getUserName().equals(username) || m.getEmail().equals(username)) {
				member = m;
				break;
			}
		}
		return member;
	}

	@Override
	public List<String> findMemberUserNamesFromHouseholdById(int householdId) {
		List<String> memberUserNameList = new ArrayList<>();
		for (Member m : memberRepo.findMembersInHouseholdById(householdId)) {
			memberUserNameList.add(m.getUserName());
		}
		return memberUserNameList;
	}

	@Override
	public long findNetWageByGUID(String memberGUID) {
		Member member = memberRepo.getById(memberGUID);
		return member.getNetWage();
	}
}
