package org.finalproject.lhms.service.impl;

import java.util.List;
import java.util.Locale;

import org.finalproject.lhms.controller.form.LoginForm;
import org.finalproject.lhms.model.Member;
import org.finalproject.lhms.repository.MemberRepository;
import org.finalproject.lhms.service.LoginService;
import org.finalproject.lhms.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

@Service
public class LoginServiceImpl implements LoginService {
	private static final Locale LOCALE = LocaleContextHolder.getLocale();
	
	@Autowired
	private MemberRepository memberRepo;
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
    private MessageSource messageSource;

	@Override
	public Validation validateUserEmailAndPassword(LoginForm loginForm, Errors errors) {
		List<Member> repoMembers = memberRepo.findAll();
		String formUser = loginForm.getLoginName();
		String formPass = loginForm.getPassword();
		Validation vlidation = Validation.INVALID;
		for (Member member : repoMembers) {
			if (member.getUserName().equals(formUser) || member.getEmail().equals(formUser)) {
				if (member.getPassword().equals(memberService.encriptPassword(formPass))) {
					vlidation = Validation.VALID;
					break;
				} else {
					errors.rejectValue("password", "error.pass.notvalid", 
							new Object[] {messageSource.getMessage("member.registration.password.label", null, LOCALE)}, 
							"The given password is invalid!");
					vlidation = Validation.PASS;
					break;
				}
			} else {
				vlidation = Validation.INVALID;
				continue;
			}
		}
		return vlidation;
	}
}
