package org.finalproject.lhms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.finalproject.lhms.model.Household;
import org.finalproject.lhms.repository.HouseholdRepository;
import org.finalproject.lhms.service.HouseholdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HouseholdServiceImpl implements HouseholdService {
	
	@Autowired
	private HouseholdRepository hhRepo;
	
	@Autowired
	private Household household;

	@Override
	public List<Integer> getHouseholdMemberCount() {
		List<Integer> householdMember = new ArrayList<Integer>();
		int lastAddedHouseholdMemberCount = getLastlyAddedHousehold().getHouseholdMemberCount();
		for (int i = 1; i <= lastAddedHouseholdMemberCount; i++) {
			householdMember.add(i);
		}		
		return householdMember;
	}

	@Override
	public void reduceMemberCountWhitOne() {
		Household currentHousehold = getLastlyAddedHousehold();
		int reducedHouseholdMemberCount = currentHousehold.getHouseholdMemberCount();
		if(currentHousehold.getHouseholdMemberCount() > 1) {
			reducedHouseholdMemberCount--;
		}
		currentHousehold.setHouseholdMemberCount(reducedHouseholdMemberCount);
		hhRepo.update(currentHousehold);
	}

	@Override
	public Household getLastlyAddedHousehold() {
		List<Household> allHousehold = hhRepo.findAll();
		return allHousehold.get(allHousehold.size() -1);
	}

	@Override
	public void deleteHouseholdById() {
		hhRepo.deleteById(getLastlyAddedHousehold().getHouseholdGUID());
	}

	@Override
	public Household findHouseholdById(int householdId) {
		List<Household> allHousehold = hhRepo.findAll();
		for (Household h : allHousehold) {
			if (h.getHouseholdId() == householdId) {
				household = h;
				break;
			}
		}
		return household;
	}
}
