package org.finalproject.lhms.service;

import java.util.List;

import org.finalproject.lhms.model.Household;

public interface HouseholdService {
	/**
	 * This method gets the actual household member count from the repository <br>
	 * and returns it in a predefined format<br>
	 * so that it can be used later on the Member Registration Page
	 * @return List<String>*/
	List<Integer> getHouseholdMemberCount();
	
	/**This method reduces the households member count whit one<br>
	 * when the cancel button is pressed and the member count is grater then one*/
	void reduceMemberCountWhitOne();
	
	/**
	 * This method gets the lastly added household from the database
	 * @return Household*/
	Household getLastlyAddedHousehold();
	
	/**This method deletes the current household<br>
	 * when there is only one household member is in the database
	 * and the cancel button is pressed*/
	void deleteHouseholdById();
	
	Household findHouseholdById(int householdId);
}
