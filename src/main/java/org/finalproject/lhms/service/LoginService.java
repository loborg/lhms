package org.finalproject.lhms.service;

import org.finalproject.lhms.controller.form.LoginForm;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

@Service
public interface LoginService {
	
	public static enum Validation{
		PASS, INVALID, VALID
	}
	
	Validation validateUserEmailAndPassword(LoginForm loginForm, Errors errors);
}
