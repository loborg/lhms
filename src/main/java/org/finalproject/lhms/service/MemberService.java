package org.finalproject.lhms.service;

import java.util.List;

import org.finalproject.lhms.controller.form.MemberForm;
import org.finalproject.lhms.model.Member;
import org.springframework.validation.Errors;

public interface MemberService {

	/**This method delete all the household member from the member table from the specific household*/
	void deleteMembersFromSpecificHousuold();
	
	/**
	 * This method examine if the selected member is already added to the database<br>
	 * And if it is, then returns an error message to display on the member registration page.
	 * @param*/
	void examineMemberExistenceWithinHousehold(MemberForm memberForm, Errors errors);
	
	/**
	 * This method examine the given User Name <br> 
	 * and determine if it is in the member database or not.<br>
	 * If it is, then it returns an error message to display on the member registration page.
	 * @param*/
	void usernameExistenceIspection(MemberForm memberForm, Errors errors);
	
	/**
	 * This method examine the given Email Address <br> 
	 * and determine if it is in the member database or not.<br>
	 * If it is, then it returns an error message to display on the member registration page.
	 * @param*/
	void emailExistenceIspection(MemberForm memberForm, Errors errors);
	
	/**
	 * This method saves a member to the actual household*/
	void saveHouseholdMemberToDatabase(Member member);
	
	/**
	 * This method encrypts the registered password, so it can not be retain from the database*/
	String encriptPassword(String password);
	
	/**
	 * Finds a single member by its user name
	 * @param*/
	Member findMemberByUserName(String username);
	
	List<String> findMemberUserNamesFromHouseholdById(int householdId);
	
	long findNetWageByGUID(String memberGUID);
}
