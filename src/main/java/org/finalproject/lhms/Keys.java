package org.finalproject.lhms;

/**In this class you can find the reusable string keys of the application*/
public class Keys {
	
	public static final String HOUSEHOLD_REGISTRATION_VIEW = "household-registration-page";
	public static final String MEMBER_REGISTRATION_VIEW = "member-registration-page";
	public static final String LANDING_PAGE_VIEW = "landing-page";
	public static final String LOGIN_PAGE_VIEW = "login-page";
	public static final String FINANCIAL_PAGE_VIEW = "financial-page";
	public static final String HOUSEHOLD_REGISTRATION_VIEW_ATR = "householdForm";
	public static final String MEMBER_REGISTRATION_VIEW_ATR = "memberForm";
	public static final String MEMBER_ALREADY_REGISTRATED_MESSAGE = "This member was already registrated!";
	public static final String LOGIN_VIEW_ATR = "loginForm";
	public static final String WELCOME_PAGE_VIEW = "welcome-page";
	public static final String PERSONAL_PAGE_VIEW = "personal";
	public static final String BASE_INCOME_ITEM = "base";
}
