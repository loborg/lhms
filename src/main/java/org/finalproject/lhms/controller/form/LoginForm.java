package org.finalproject.lhms.controller.form;

public class LoginForm {
	
	private String loginName;
	
	private String password;
	private Action action;
	
	public static enum Action {
		LOGIN, CANCEL
	}

	
	public String getLoginName() {
		return loginName;
	}

	public String getPassword() {
		return password;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
