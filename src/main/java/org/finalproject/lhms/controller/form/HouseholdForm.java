package org.finalproject.lhms.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class HouseholdForm {

	private int householdId;
	
	@NotEmpty
	private String householdName;
	
	private String householdAddress;
	
	@NotNull
	private int housholdMemberCount;
	
	private String householdGUID;
	
	@NotNull
	private Action action;
	
	public static enum Action {
        REGISTER, CANCEL
    }

	public int getHouseholdId() {
		return householdId;
	}

	public String getHouseholdName() {
		return householdName;
	}

	public String getHouseholdAddress() {
		return householdAddress;
	}

	public int getHousholdMemberCount() {
		return housholdMemberCount;
	}

	public String getHouseholdGUID() {
		return householdGUID;
	}

	public Action getAction() {
		return action;
	}

	public void setHouseholdId(int householdId) {
		this.householdId = householdId;
	}

	public void setHouseholdName(String householdName) {
		this.householdName = householdName;
	}

	public void setHouseholdAddress(String householdAddress) {
		this.householdAddress = householdAddress;
	}

	public void setHousholdMemberCount(int housholdMemberCount) {
		this.housholdMemberCount = housholdMemberCount;
	}

	public void setHouseholdGUID(String householdGUID) {
		this.householdGUID = householdGUID;
	}

	public void setAction(Action action) {
		this.action = action;
	}
}
