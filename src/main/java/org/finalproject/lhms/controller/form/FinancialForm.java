package org.finalproject.lhms.controller.form;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class FinancialForm {
	
	private int itemId;
	private Date intakeDate;
	private String incomeItem;
	private int incomeAmount;
	private String expenseItem;
	private int expenseAmount;
	private String memberGUID;
	private Action action;
	
	public enum Action{
		ADD, LOGOUT
	}

	public int getItemId() {
		return itemId;
	}

	public Date getIntakeDate() {
		return intakeDate;
	}

	public String getIncomeItem() {
		return incomeItem;
	}

	public int getIncomeAmount() {
		return incomeAmount;
	}

	public String getExpenseItem() {
		return expenseItem;
	}

	public int getExpenseAmount() {
		return expenseAmount;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public void setIntakeDate(Date intakeDate) {
		this.intakeDate = intakeDate;
	}

	public void setIncomeItem(String incomeItem) {
		this.incomeItem = incomeItem;
	}

	public void setIncomeAmount(int incomeAmount) {
		this.incomeAmount = incomeAmount;
	}

	public void setExpenseItem(String expenseItem) {
		this.expenseItem = expenseItem;
	}

	public void setExpenseAmount(int expenseAmount) {
		this.expenseAmount = expenseAmount;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public String getMemberGUID() {
		return memberGUID;
	}

	public void setMemberGUID(String memberGUID) {
		this.memberGUID = memberGUID;
	}
}
