package org.finalproject.lhms.controller.form;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class MemberForm {
	
	private int memberId;
	private int householdId;
	
	@NotEmpty
	private String firstName;
	
	@NotEmpty
	private String lastName;
	private String phoneNumber;
	
	@NotEmpty
	@Email
	@Size(max=64)
	private String email;
	
	@NotEmpty
	@Email
	@Size(max=64)
	private String emailConfirmation;
	private boolean salary;
	private long netWage;
	
	@NotEmpty
	@Size(max=16)
	private String userName;
	
	@NotEmpty
	@Size(min=6, max=16)
	private String password;
	
	@NotEmpty
	@Size(min=6, max=16)
	private String passwordConfirmation;
	private String memberGUID;
	
	@NotNull
	private Action action;
	
	public static enum Action {
        ADD, CANCEL
    }

	public int getMemberId() {
		return memberId;
	}

	public int getHouseholdId() {
		return householdId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public String getEmailConfirmation() {
		return emailConfirmation;
	}

	public boolean isSalary() {
		return salary;
	}

	public long getNetWage() {
		return netWage;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	public String getMemberGUID() {
		return memberGUID;
	}
	
	public Action getAction() {
		return action;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public void setHouseholdId(int householdId) {
		this.householdId = householdId;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEmailConfirmation(String emailConfirmation) {
		this.emailConfirmation = emailConfirmation;
	}

	public void setSalary(boolean salary) {
		this.salary = salary;
	}

	public void setNetWage(long netWage) {
		this.netWage = netWage;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}

	public void setMemberGUID(String memberGUID) {
		this.memberGUID = memberGUID;
	}
	
	public void setAction(Action action) {
		this.action = action;
	}
}
