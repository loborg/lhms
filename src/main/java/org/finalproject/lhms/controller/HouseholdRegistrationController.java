package org.finalproject.lhms.controller;

import static org.finalproject.lhms.Keys.HOUSEHOLD_REGISTRATION_VIEW;
import static org.finalproject.lhms.Keys.HOUSEHOLD_REGISTRATION_VIEW_ATR;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.finalproject.lhms.controller.form.HouseholdForm;
import org.finalproject.lhms.controller.form.HouseholdForm.Action;
import org.finalproject.lhms.controller.validator.HouseholdRegistrationValidator;
import org.finalproject.lhms.model.Household;
import org.finalproject.lhms.repository.HouseholdRepository;
import org.finalproject.lhms.service.HouseholdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/household-registration-page.html")
public class HouseholdRegistrationController {
	
	@Autowired
	private HouseholdRegistrationValidator validator;
	
	@Autowired
	private HouseholdRepository repo;
	
	@Autowired
	private HouseholdService service;
	
	@Resource
    private ConversionService conversionService;
	
	@GetMapping()
	public String getHouseholdRegistrationPage(@ModelAttribute(HOUSEHOLD_REGISTRATION_VIEW_ATR) HouseholdForm householdForm) {
		return HOUSEHOLD_REGISTRATION_VIEW;
	}
	
	@PostMapping()
	public String postHouseholdRegistration(Model model, 
			@Valid @ModelAttribute(HOUSEHOLD_REGISTRATION_VIEW_ATR) HouseholdForm householdForm, 
			BindingResult bindingResult) {
		if (householdForm.getAction() == Action.REGISTER){ //Registers a new household to the database, and redirects to the member registration page
			if(bindingResult.hasErrors()) { //If household has errors reloads the registration page whit the specific errormessages
				return HOUSEHOLD_REGISTRATION_VIEW;
			}
			Household house = conversionService.convert(householdForm, Household.class);
			repo.create(house);
			
			return "redirect:/member-registration/?GUID=" + service.getLastlyAddedHousehold().getHouseholdGUID();
		} else {
			return "redirect:/"; //If the cancel button is pressed then redirects to the Landingpage
		}
	}
	
	@InitBinder(HOUSEHOLD_REGISTRATION_VIEW_ATR)
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(validator);
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
}
