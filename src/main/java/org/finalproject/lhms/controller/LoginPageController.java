package org.finalproject.lhms.controller;


import static org.finalproject.lhms.Keys.LOGIN_PAGE_VIEW;
import static org.finalproject.lhms.Keys.LOGIN_VIEW_ATR;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.finalproject.lhms.controller.form.LoginForm;
import org.finalproject.lhms.controller.validator.LoginValidator;
import org.finalproject.lhms.model.Household;
import org.finalproject.lhms.model.Member;
import org.finalproject.lhms.service.HouseholdService;
import org.finalproject.lhms.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping("/")
@SessionAttributes(value= {"member", "household"})
public class LoginPageController {
	
	@Autowired
	private LoginValidator validators;
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private HouseholdService householdService;
	
	@GetMapping
	public String getLoginPage(@ModelAttribute(LOGIN_VIEW_ATR) LoginForm loginForm, HttpServletRequest request) {
		Member member = (Member) request.getSession().getAttribute("member");
		if (member != null) {
			return "redirect:/welcome/?memberGUID=" +member.getMemberGUID();
		} else {
			return LOGIN_PAGE_VIEW;
		}
	}
	
	@PostMapping
	public String login(Model model, @Valid @ModelAttribute(LOGIN_VIEW_ATR) LoginForm loginForm, BindingResult bindingResult) {
		
		switch (loginForm.getAction()) {
		case LOGIN:
			if (bindingResult.hasErrors()) {
				return LOGIN_PAGE_VIEW;
			}
			Member member = memberService.findMemberByUserName(loginForm.getLoginName());
			Household household = householdService.findHouseholdById(member.getHouseholdId());
			model.addAttribute("member", member);
			model.addAttribute("household", household);
			return "redirect:/welcome/?memberGUID=" + member.getMemberGUID();
		case CANCEL:
			return "redirect:/household-registration-page.html";
		default:
			return "redirect:/";
		}
	}
	
	@InitBinder(LOGIN_VIEW_ATR)
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		binder.addValidators(validators);	
	}
}
