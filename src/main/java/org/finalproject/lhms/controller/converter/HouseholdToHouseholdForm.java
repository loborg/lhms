package org.finalproject.lhms.controller.converter;

import org.finalproject.lhms.controller.form.HouseholdForm;
import org.finalproject.lhms.model.Household;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class HouseholdToHouseholdForm implements Converter<Household, HouseholdForm> {

	@Override
	public HouseholdForm convert(Household source) {
		HouseholdForm houseForm = new HouseholdForm();
		houseForm.setHouseholdId(source.getHouseholdId());
		houseForm.setHouseholdName(source.getHouseholdName());
		houseForm.setHouseholdAddress(source.getHouseholdAddress());
		houseForm.setHousholdMemberCount(source.getHouseholdMemberCount());
		houseForm.setHouseholdGUID(source.getHouseholdGUID());
		return houseForm;
	}

}
