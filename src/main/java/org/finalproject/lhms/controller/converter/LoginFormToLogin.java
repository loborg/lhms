package org.finalproject.lhms.controller.converter;

import org.finalproject.lhms.controller.form.LoginForm;
import org.finalproject.lhms.model.Login;
import org.springframework.core.convert.converter.Converter;

public class LoginFormToLogin implements Converter<LoginForm, Login> {

	@Override
	public Login convert(LoginForm source) {
		Login model = new Login();
		model.setLoginName(source.getLoginName());
		model.setPassword(source.getPassword());
		return model;
	}

}
