package org.finalproject.lhms.controller.converter;

import org.finalproject.lhms.controller.form.FinancialForm;
import org.finalproject.lhms.model.Financial;
import org.springframework.core.convert.converter.Converter;

public class FinancialToFinancialForm implements Converter<Financial, FinancialForm> {

	@Override
	public FinancialForm convert(Financial source) {
		FinancialForm financialForm = new FinancialForm();
		financialForm.setItemId(source.getItemId());
		financialForm.setIntakeDate(source.getIntakeDate());
		financialForm.setIncomeItem(source.getIncomeItem());
		financialForm.setExpenseItem(source.getExpenseItem());
		financialForm.setIncomeAmount(source.getIncomeAmount());
		financialForm.setExpenseAmount(source.getExpenseAmount());
		financialForm.setMemberGUID(source.getMemberGUID());
		return financialForm;
	}
}
