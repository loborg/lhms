package org.finalproject.lhms.controller.converter;

import org.finalproject.lhms.controller.form.MemberForm;
import org.finalproject.lhms.model.Member;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class MemberFormToMember implements Converter<MemberForm, Member> {

	@Override
	public Member convert(MemberForm source) {
		Member member = new Member();
		member.setMemberId(source.getMemberId());
		member.setHouseholdId(source.getHouseholdId());
		member.setFirstName(source.getFirstName());
		member.setLastName(source.getLastName());
		member.setPhoneNumber(source.getPhoneNumber());
		member.setEmail(source.getEmail());
		member.setEmailConfirmation(source.getEmailConfirmation());
		member.setSalary(source.isSalary());
		member.setNetWage(source.getNetWage());
		member.setUserName(source.getUserName());
		member.setPassword(source.getPassword());
		member.setPasswordConfirmation(source.getPasswordConfirmation());
		member.setMemberGUID(source.getMemberGUID());
		return member;
	}

}
