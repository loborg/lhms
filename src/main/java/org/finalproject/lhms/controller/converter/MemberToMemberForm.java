package org.finalproject.lhms.controller.converter;

import org.finalproject.lhms.controller.form.MemberForm;
import org.finalproject.lhms.model.Member;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class MemberToMemberForm implements Converter<Member, MemberForm> {

	@Override
	public MemberForm convert(Member source) {
		MemberForm memberForm = new MemberForm();
		memberForm.setMemberId(source.getMemberId());
		memberForm.setHouseholdId(source.getHouseholdId());
		memberForm.setFirstName(source.getFirstName());
		memberForm.setLastName(source.getLastName());
		memberForm.setPhoneNumber(source.getPhoneNumber());
		memberForm.setEmail(source.getEmail());
		memberForm.setEmailConfirmation(source.getEmailConfirmation());
		memberForm.setSalary(source.isSalary());
		memberForm.setNetWage(source.getNetWage());
		memberForm.setUserName(source.getUserName());
		memberForm.setPassword(source.getPassword());
		memberForm.setPasswordConfirmation(source.getPasswordConfirmation());
		memberForm.setMemberGUID(source.getMemberGUID());
		return memberForm;
	}

}
