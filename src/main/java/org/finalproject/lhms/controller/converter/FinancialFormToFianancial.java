package org.finalproject.lhms.controller.converter;

import org.finalproject.lhms.controller.form.FinancialForm;
import org.finalproject.lhms.model.Financial;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class FinancialFormToFianancial implements Converter<FinancialForm, Financial> {

	@Override
	public Financial convert(FinancialForm source) {
		Financial financial = new Financial();
		financial.setItemId(source.getItemId());
		financial.setIntakeDate(source.getIntakeDate());
		financial.setIncomeItem(source.getIncomeItem());
		financial.setExpenseItem(source.getExpenseItem());
		financial.setIncomeAmount(source.getIncomeAmount());
		financial.setExpenseAmount(source.getExpenseAmount());
		financial.setMemberGUID(source.getMemberGUID());
		return financial;
	}
}
