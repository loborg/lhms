package org.finalproject.lhms.controller.converter;

import org.finalproject.lhms.controller.form.LoginForm;
import org.finalproject.lhms.model.Login;
import org.springframework.core.convert.converter.Converter;

public class LoginToLoginForm implements Converter<Login, LoginForm> {

	@Override
	public LoginForm convert(Login source) {
		LoginForm form = new LoginForm();
		form.setLoginName(source.getLoginName());
		form.setPassword(source.getPassword());
		return form;
	}
}
