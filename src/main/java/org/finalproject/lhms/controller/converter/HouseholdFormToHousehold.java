package org.finalproject.lhms.controller.converter;

import org.finalproject.lhms.controller.form.HouseholdForm;
import org.finalproject.lhms.model.Household;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class HouseholdFormToHousehold implements Converter<HouseholdForm, Household>{

	@Override
	public Household convert(HouseholdForm source) {
		Household house = new Household();
		house.setHouseholdId(source.getHouseholdId());
		house.setHouseholdName(source.getHouseholdName());
		house.setHouseholdAddress(source.getHouseholdAddress());
		house.setHouseholdMemberCount(source.getHousholdMemberCount());
		house.setHouseholdGUID(source.getHouseholdGUID());
		return house;
	}
}
