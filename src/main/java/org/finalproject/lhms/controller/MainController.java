package org.finalproject.lhms.controller;

import static org.finalproject.lhms.Keys.FINANCIAL_PAGE_VIEW;
import static org.finalproject.lhms.Keys.PERSONAL_PAGE_VIEW;
import static org.finalproject.lhms.Keys.WELCOME_PAGE_VIEW;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.finalproject.lhms.controller.form.FinancialForm;
import org.finalproject.lhms.controller.validator.FinancialPageValidator;
import org.finalproject.lhms.model.Financial;
import org.finalproject.lhms.model.Household;
import org.finalproject.lhms.model.Member;
import org.finalproject.lhms.model.TableItem;
import org.finalproject.lhms.repository.MemberRepository;
import org.finalproject.lhms.service.FinancialService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;

@Controller
@SessionAttributes(value= {"member", "household"})
public class MainController {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MemberRepository memberRepository;
	
	@Autowired
	private FinancialService financialService;
	
	@Autowired
	private FinancialPageValidator validators;
	
	@Autowired
	private ConversionService conversionService;

	@GetMapping("/welcome")
	public String welcomePage(Model model, @RequestParam(value = "memberGUID", required=false) String memberGUID, HttpServletRequest request) {
		try {
			Member member = (Member) request.getSession().getAttribute("member");
			Household household = (Household) request.getSession().getAttribute("household");
			logger.warn("The actual user is: " + member.getFirstName() +" "+ member.getLastName() + " whit the GUID: " + member.getMemberGUID() + " in " + household.getHouseholdName());
		} catch (NullPointerException e) {
			return "redirect:/";
		}
		return WELCOME_PAGE_VIEW;
	}
	
	@GetMapping("/personal")
	public String personalPage(HttpServletRequest request) {
		try {
			Member member = (Member) request.getSession().getAttribute("member");
			Household household = (Household) request.getSession().getAttribute("household");
			logger.warn("The actual user is: " + member.getFirstName() +" "+ member.getLastName() + " whit the GUID: " + member.getMemberGUID() + " in " + household.getHouseholdName());
		} catch (NullPointerException e) {
			return "redirect:/";
		}
		return PERSONAL_PAGE_VIEW; 
	}
	
	@GetMapping("/financial/*")
	public String financialPage(Model model, @ModelAttribute("financialForm") FinancialForm financialForm, HttpServletRequest request) {
		try {
			Household household = (Household) request.getSession().getAttribute("household");
			model.addAttribute("members", memberRepository.findMembersInHouseholdById(household.getHouseholdId()));

			List<TableItem> listOfTableItems = financialService.findAllTableItem(household.getHouseholdId());
			model.addAttribute("tableItems", listOfTableItems);
		} catch (NullPointerException e) {
			return "redirect:/";
		}
		
		return FINANCIAL_PAGE_VIEW;
	}
	
	@PostMapping("/financial/*")
	public String postFinancialData(Model model, 
			@Valid @ModelAttribute("financialForm") FinancialForm financialForm, 
			BindingResult bindingResult, HttpServletRequest request,
			SessionStatus sessionStatus) {
		
		Member member = (Member) request.getSession().getAttribute("member");
		Household household = (Household) request.getSession().getAttribute("household");
		model.addAttribute("members", memberRepository.findMembersInHouseholdById(household.getHouseholdId()));
		financialFormPostLogs(financialForm, member);
		
		switch (financialForm.getAction()) {
		case ADD:
			if (bindingResult.hasErrors()) {
				return FINANCIAL_PAGE_VIEW;
			}
			
			Financial financial = conversionService.convert(financialForm, Financial.class);
			financialService.saveFinancialData(financial);
			
			
			List<TableItem> listOfTableItems = financialService.findAllTableItem(household.getHouseholdId());
			model.addAttribute("tableItems", listOfTableItems);
			logger.warn("The First table item is: " + listOfTableItems.get(0).getMember());
			logger.warn("The Last table item is: " + listOfTableItems.get(listOfTableItems.size()-1).getMember());
			model.addAttribute("tableItems", listOfTableItems);
			break;
		case LOGOUT:
			sessionStatus.setComplete();
			return "redirect:/financial/" + member.getUserName();
		default:
			break;
		}
		
		return "redirect:/financial/" + member.getUserName();
	}
	
	@InitBinder("financialForm")
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
		binder.addValidators(validators);	
	}
	
	@ExceptionHandler
	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
	public String handleException(MySQLSyntaxErrorException e) {
		return "error-pages/error-no-db";
	}
	
	private void financialFormPostLogs(FinancialForm financialForm, Member member) {
		logger.warn("Income: " + financialForm.getIncomeItem() + " Amount: " + financialForm.getIncomeAmount());
		logger.warn("Expense: " + financialForm.getExpenseItem() + " Amount: " + financialForm.getExpenseAmount());
		logger.warn("Intake date: " + financialForm.getIntakeDate());
		logger.warn("Added by: " + member.getUserName() + " GUID: " + financialForm.getMemberGUID());
	}
}
