package org.finalproject.lhms.controller;

import static org.finalproject.lhms.Keys.LANDING_PAGE_VIEW;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Deprecated
@Controller
@RequestMapping("/f")
public class LandingPageController {
	
	@GetMapping()
	public String getLandingPage() {
		return LANDING_PAGE_VIEW;
	}

}
