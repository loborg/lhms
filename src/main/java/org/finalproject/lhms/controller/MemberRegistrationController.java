package org.finalproject.lhms.controller;

import static org.finalproject.lhms.Keys.MEMBER_REGISTRATION_VIEW;
import static org.finalproject.lhms.Keys.MEMBER_REGISTRATION_VIEW_ATR;

import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.finalproject.lhms.controller.form.MemberForm;
import org.finalproject.lhms.controller.validator.MemberRegistrationValidator;
import org.finalproject.lhms.model.Member;
import org.finalproject.lhms.service.HouseholdService;
import org.finalproject.lhms.service.MemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/member-registration")
public class MemberRegistrationController {
	
	private int memberCounter = 0;
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MemberRegistrationValidator validators;
	
	@Autowired
	private HouseholdService hhService;
	
	@Autowired
	private MemberService hhMemberService;
	
	@Resource
    private ConversionService conversionService;
	
	@ModelAttribute("householdMembers")
	private List<Integer> householdMemberCount(){
		logger.warn("Repo is colecting data...");
		return hhService.getHouseholdMemberCount();
	} 
	
	@GetMapping
	public String getMemberRegistrationPage(@RequestParam(value="GUID") String GUID, @ModelAttribute(MEMBER_REGISTRATION_VIEW_ATR) MemberForm memberForm) {
		for (Integer s : householdMemberCount()) {
			logger.info(s.toString()); //Loging the member count texts
		}
		return MEMBER_REGISTRATION_VIEW;
	}
	
	@PostMapping
	public String registerMemberToHousehold(@Valid @ModelAttribute(MEMBER_REGISTRATION_VIEW_ATR) MemberForm memberForm, BindingResult bindingResult) {
		switch(memberForm.getAction()) {
			case ADD:
				if (bindingResult.hasErrors()) {
					logger.warn("There are errors in the member registration form!");
					return MEMBER_REGISTRATION_VIEW;
				}
				
				if (householdMemberCount().size() >= 1) {
					logger.warn("ADD if Household membercount are grater then one.");
					Member member = conversionService.convert(memberForm, Member.class);
					hhMemberService.saveHouseholdMemberToDatabase(member);
					memberCounter++;
					
					if (memberCounter == householdMemberCount().size()) {
						memberCounter = 0;
						logger.warn("MemberCounterMember: " + memberCounter + " MemberCountHousehold: " + householdMemberCount().size());
						return "redirect:/";
					} else {
						return "redirect:/member-registration/?GUID=" + hhService.getLastlyAddedHousehold().getHouseholdGUID();
					}
				}
			case CANCEL: 
				if (householdMemberCount().size() > 1) {
					hhService.reduceMemberCountWhitOne();
					logger.warn("Cancel if member count biger then one");
					return "redirect:/member-registration/?GUID=" + hhService.getLastlyAddedHousehold().getHouseholdGUID();
				} else {
					logger.warn("Cancel if only one member");
					hhMemberService.deleteMembersFromSpecificHousuold();
					hhService.deleteHouseholdById();
					return "redirect:/";
				}
			default:
				logger.warn("Cancel default");
				break;
		}
		logger.warn("Default redirect");
		return "redirect:/";
	}
	
	@InitBinder(MEMBER_REGISTRATION_VIEW_ATR)
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		binder.addValidators(validators);	
	}
}
