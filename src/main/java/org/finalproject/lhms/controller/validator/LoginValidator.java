package org.finalproject.lhms.controller.validator;

import org.finalproject.lhms.controller.form.LoginForm;
import org.finalproject.lhms.service.LoginService;
import org.finalproject.lhms.service.LoginService.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class LoginValidator implements Validator {
	
	@Autowired
	private LoginService loginService;

	@Override
	public boolean supports(Class<?> clazz) {
		return LoginForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		LoginForm loginForm = (LoginForm) target;
		if (loginForm.getLoginName() == null) {
			errors.rejectValue("loginName", "error.notemtiy.login", new Object[] {"Login"}, "User name can not be empty");
		} else if (loginForm.getPassword() == null) {
			errors.rejectValue("password", "error.notemtiy.pass", new Object[] {"Pass"}, "Password can not be empty");
		} else {
			if (loginService.validateUserEmailAndPassword(loginForm, errors) == Validation.INVALID){
				errors.rejectValue("loginName", "error.nouser", null, "No such member");
				errors.rejectValue("password", "error.blank", null, null);
			}	
		}
	}
}
