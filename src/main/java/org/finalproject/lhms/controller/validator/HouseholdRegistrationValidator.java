package org.finalproject.lhms.controller.validator;

import org.finalproject.lhms.controller.form.HouseholdForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class HouseholdRegistrationValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return HouseholdForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors){
		HouseholdForm householdForm = (HouseholdForm) target; 
		ValidationUtils.rejectIfEmpty(errors, "housholdMemberCount", "error.notempty");
		if (householdForm.getHousholdMemberCount() < 1) {
			errors.rejectValue("housholdMemberCount", "error.number");
		}
	}
}
