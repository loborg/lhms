package org.finalproject.lhms.controller.validator;

import static org.finalproject.lhms.Keys.MEMBER_ALREADY_REGISTRATED_MESSAGE;

import java.util.Locale;

import org.finalproject.lhms.controller.form.MemberForm;
import org.finalproject.lhms.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class MemberRegistrationValidator implements Validator {

	private static final Locale LOCALE = LocaleContextHolder.getLocale();

	@Autowired
    private MessageSource messageSource;
	
	@Autowired
	private MemberService memberService;

	@Override
	public boolean supports(Class<?> clazz) {
		return MemberForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		MemberForm memberForm = (MemberForm) target;
	
		//Member select validation
		memberService.examineMemberExistenceWithinHousehold(memberForm, errors);
		
		//First name validation
		if (memberForm.getFirstName() == null) {
			errors.rejectValue("firstName", "error.notempty.member", 
					new Object[] {messageSource.getMessage("member.registration.firstname.label", new Object[] {}, LOCALE)}, 
					MEMBER_ALREADY_REGISTRATED_MESSAGE);
		}
		
		//Last name validation
		if (memberForm.getLastName() == null) {
			errors.rejectValue("lastName", "error.notempty.member", 
					new Object[] {messageSource.getMessage("member.registration.lastname.label", new Object[] {}, LOCALE)}, 
					MEMBER_ALREADY_REGISTRATED_MESSAGE);
		}
		
		//UserID validation
		if (memberForm.getUserName() == null) {
			errors.rejectValue("userName", "error.notempty.member", 
					new Object[] {messageSource.getMessage("member.registration.loginname.label", new Object[] {}, LOCALE)}, 
					MEMBER_ALREADY_REGISTRATED_MESSAGE);
		} else if (memberForm.getUserName().length() > 16) { //UserID length validation
			errors.rejectValue("userName", "error.max16.size", 
					new Object[] {messageSource.getMessage("member.registration.loginname.label", new Object[] {}, LOCALE)}, 
					MEMBER_ALREADY_REGISTRATED_MESSAGE);
		} else {
			memberService.usernameExistenceIspection(memberForm, errors);
		}
		
		//Email and Email Confirmation equality validation
		if (memberForm.getEmail() != null && memberForm.getEmailConfirmation() != null) {
			if (!memberForm.getEmail().equals(memberForm.getEmailConfirmation())) {
				errors.rejectValue("emailConfirmation", "error.email.confirmation.failed");
			}
			
			//Email length validation
			if (memberForm.getEmail().length() > 64) {
				errors.rejectValue("email", "error.max64.size", 
						new Object[] {messageSource.getMessage("member.registration.email.label", new Object[] {}, LOCALE)}, 
						MEMBER_ALREADY_REGISTRATED_MESSAGE);
			}
			
			//Email Confirmation length validation
			if (memberForm.getEmailConfirmation().length() > 64) {
				errors.rejectValue("emailConfirmation", "error.max64.size", 
						new Object[] {messageSource.getMessage("member.registration.emailconf.label", new Object[] {}, LOCALE)}, 
						MEMBER_ALREADY_REGISTRATED_MESSAGE);
			}
			
			memberService.emailExistenceIspection(memberForm, errors);
		} 
		
		//Email null validation
		if (memberForm.getEmail() == null) {
			errors.rejectValue("email", "error.notempty.member", 
					new Object[] {messageSource.getMessage("member.registration.email.label", new Object[] {}, LOCALE)}, 
					MEMBER_ALREADY_REGISTRATED_MESSAGE);
		} 
		
		//Email Confirmation null validation
		if (memberForm.getEmailConfirmation() == null) {
			errors.rejectValue("emailConfirmation", "error.notempty.member", 
					new Object[] {messageSource.getMessage("member.registration.emailconf.label", new Object[] {}, LOCALE)}, 
					MEMBER_ALREADY_REGISTRATED_MESSAGE);
		}
		
		//Password and Password Confirmation validation
		if (memberForm.getPassword() != null && memberForm.getPasswordConfirmation() != null) {
			if (!memberForm.getPassword().equals(memberForm.getPasswordConfirmation())) {
				errors.rejectValue("passwordConfirmation", "error.password.confirmation.failed");
			}
			
			//Password length validation
			if (memberForm.getPassword().length() < 6 || memberForm.getPassword().length() > 16) {
				errors.rejectValue("password", "error.between.size", 
						new Object[] {messageSource.getMessage("member.registration.password.label", new Object[] {}, LOCALE)}, 
						MEMBER_ALREADY_REGISTRATED_MESSAGE);
			}
			
			//Password Confirmation validation
			if (memberForm.getPasswordConfirmation().length() < 6 || memberForm.getPasswordConfirmation().length() > 16) {
				errors.rejectValue("passwordConfirmation", "error.between.size", 
						new Object[] {messageSource.getMessage("member.registration.passconfirm.label", new Object[] {}, LOCALE)}, 
						MEMBER_ALREADY_REGISTRATED_MESSAGE);
			}
		}
		
		//Password null validation
		if (memberForm.getPassword() == null) {
			errors.rejectValue("password", "error.notempty.member", 
					new Object[] {messageSource.getMessage("member.registration.password.label", new Object[] {}, LOCALE)}, 
					MEMBER_ALREADY_REGISTRATED_MESSAGE);
		}
		
		//Password Confirmation validation
		if (memberForm.getPasswordConfirmation() == null) {
			errors.rejectValue("passwordConfirmation", "error.notempty.member", 
					new Object[] {messageSource.getMessage("member.registration.passconfirm.label", new Object[] {}, LOCALE)}, 
					MEMBER_ALREADY_REGISTRATED_MESSAGE);
		}
		
	}
}
