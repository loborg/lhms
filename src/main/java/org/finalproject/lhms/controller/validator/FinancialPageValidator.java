package org.finalproject.lhms.controller.validator;

import org.finalproject.lhms.controller.form.FinancialForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class FinancialPageValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return FinancialForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		FinancialForm financialForm = (FinancialForm) target;
		
		if (financialForm.getIntakeDate() == null) {
			errors.rejectValue("intakeDate", "error.financial.intakeDate");
		}
		
		if (financialForm.getIncomeItem() == null && financialForm.getExpenseItem() == null) {
			errors.rejectValue("incomeItem", "error.financial.atlistOneField");
		}
		
		if (financialForm.getIncomeAmount() == 0 && financialForm.getExpenseAmount() == 0) {
			errors.rejectValue("incomeAmount", "error.financial.atlistOneField");
		}
	}
}
