CREATE TABLE IF NOT EXISTS household (
	householdId int AUTO_INCREMENT PRIMARY KEY,
	householdName  varchar(255),
	householdAddress  varchar(255),
	householdMemberCount  smallint(10),
	householdGUID varchar(255) UNIQUE 
);

CREATE TABLE IF NOT EXISTS member (
	memberId INT,
	householdId INT,
	firstName VARCHAR(255),
	lastName VARCHAR(255),
	phoneNumber VARCHAR(255),
	email VARCHAR(64),
	netWage LONG,
	userName VARCHAR(16),
	password VARCHAR(255),
	memberGUID VARCHAR (255) PRIMARY KEY,
	FOREIGN KEY (householdId) REFERENCES household(householdId)
);

CREATE TABLE IF NOT EXISTS financial (
	itemId INT AUTO_INCREMENT PRIMARY KEY,
	intakeDate DATE,
	incomeItem VARCHAR(255),
	incomeAmount INT,
	expenseItem VARCHAR(255),
	expenseAmount INT,
	memberGUID VARCHAR(255),
	FOREIGN KEY (memberGUID) REFERENCES member(memberGUID)
);