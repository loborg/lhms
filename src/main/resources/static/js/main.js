/**
 * 
 */

function hideTable() {
            function addNewStyle(newStyle) {
              var styleElement = document.getElementById('hide');
              if (!styleElement) {
                styleElement = document.createElement('style');
                styleElement.type = 'text/css';
                styleElement.id = 'hide';
                document.getElementsByTagName('head')[0].appendChild(styleElement);
              }
              styleElement.appendChild(document.createTextNode(newStyle));
            }
            addNewStyle('div#tableView {display:none;}');
          }

function showTable() {
  function addNewStyle(newStyle) {
    var styleElement = document.getElementById('hide');
    if (!styleElement) {
      styleElement = document.createElement('style');
      styleElement.type = 'text/css';
      styleElement.id = 'hide';
      document.getElementsByTagName('head')[0].appendChild(styleElement);
    }
    styleElement.appendChild(document.createTextNode(newStyle));
  }
  addNewStyle('div#tableView {display:block;}');
}