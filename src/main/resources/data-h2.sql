INSERT INTO household (
	householdName, 
	householdAddress, 
	householdMemberCount, 
	householdGUID) 
VALUES (
	'Base', 
	'', 
	1, 
	'a697f1df-7e83-4751-87cd-20c4babaf85b');
	
INSERT INTO member (
	memberId,
	householdId,
	firstName,
	lastName,
	phoneNumber,
	email,
	netWage,
	userName,
	password,
	memberGUID)
VALUES (
	1, 
	1, 
	'Admin', 
	'Admin', 
	'', 
	'admin@loborg.hu', 
	0, 
	'admin', 
	'[:Y8W6', 
	'5d135dcc-6e8c-41d7-a656-72515b7c2db3');